public class Dragonstone.Startup.GeminiWrite.Backend {
	
	public static void setup_store(Dragonstone.SuperRegistry super_registry){
		super_registry.stores.add_resource_store("gemini+write://",new Dragonstone.Store.GeminiWrite());
	}
}
