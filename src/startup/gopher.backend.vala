public class Dragonstone.Startup.Gopher.Backend {
	
	public static void setup_mimetypes(Dragonstone.SuperRegistry super_registry){
		super_registry.mimeguesser.add_type(".gopher","text/gopher");
		super_registry.mimeguesser.add_type(".gph","text/gopher");
	}
	
	public static void setup_store(Dragonstone.SuperRegistry super_registry){
		var store = new Dragonstone.Store.Gopher.with_mimeguesser(super_registry.mimeguesser, super_registry.gopher_type_registry);
		super_registry.stores.add_resource_store("gopher://",store);
	}
	
	public static void setup_uri_autocompletion(Dragonstone.SuperRegistry super_registry){
		var uri_autoprefixer = super_registry.uri_autoprefixer;
		uri_autoprefixer.add("gopher.","gopher://gopher.");
		uri_autoprefixer.add("gopher:","gopher://");
		uri_autoprefixer.add("gopher://","gopher://");
	}
}
