public class Dragonstone.Startup.File.Backend {
	
	public static void setup_store(Dragonstone.SuperRegistry super_registry){
			super_registry.stores.add_resource_store("file://",new Dragonstone.Store.File.with_mimeguesser(super_registry.mimeguesser));
	}
	
	public static void setup_uri_autocompletion(Dragonstone.SuperRegistry super_registry){
		var uri_autoprefixer = super_registry.uri_autoprefixer;
		uri_autoprefixer.add("file:","file://");
		uri_autoprefixer.add("file://","file://");
		uri_autoprefixer.add("/","file:///");
		uri_autoprefixer.add("~/","file://"+GLib.Environment.get_home_dir()+"/");
	}
	
}
