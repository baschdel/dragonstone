public class Dragonstone.Startup.GeminiUpload.Backend {
	
	public static void setup_store(Dragonstone.SuperRegistry super_registry){
		var store = new Dragonstone.Store.GeminiUpload();
		super_registry.stores.add_resource_store("gemini+upload://",store);
	}
}
