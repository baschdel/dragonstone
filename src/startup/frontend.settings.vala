public class Dragonstone.Startup.Frontend.Settings {
	
	public static void register_default_settings(Dragonstone.Interface.Settings.Provider settings_provider){
		print("[startup][frontend][settings] register_default_settings()\n");
		settings_provider.write_object("settings.frontend.kv","
			new_tab_uri: test://
		");
	}
}
