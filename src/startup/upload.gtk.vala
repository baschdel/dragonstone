public class Dragonstone.Startup.Upload.Gtk {
	public static void setup_views(Dragonstone.SuperRegistry super_registry){
		var view_registry = super_registry.gtk_view_registry;
		var translation = super_registry.translation;
		var mimeguesser = super_registry.mimeguesser;
		var tempfilebase = GLib.Environment.get_tmp_dir()+"/";
		print("[startup][upload][gtk] setup_views()\n");
		view_registry.add_view("upload.file",() => { return new Dragonstone.GtkUi.View.UploadFile(translation,mimeguesser); });
		view_registry.add_view("upload.text",() => {
			var tempfilepath = tempfilebase+"dragonstone_upload_"+GLib.Uuid.string_random();
			return new Dragonstone.GtkUi.View.UploadText(tempfilepath,translation,mimeguesser);
		});
		view_registry.add_rule(new Dragonstone.GtkUi.LegacyViewRegistryRule("interactive/upload","upload.file"));
		view_registry.add_rule(new Dragonstone.GtkUi.LegacyViewRegistryRule("interactive/upload/text","upload.text"));
	}
}
