public class Dragonstone.Startup.GopherWrite.Backend {
	
	public static void setup_gophertypes(Dragonstone.SuperRegistry super_registry){
		super_registry.gopher_type_registry.add(new Dragonstone.Registry.GopherTypeRegistryEntry('w',null,"gopher+writet://{host}:{port}/{selector}"));
	}
	
	public static void setup_store(Dragonstone.SuperRegistry super_registry){
		var store_registry = super_registry.stores;
		var store = new Dragonstone.Store.GopherWrite();
		store_registry.add_resource_store("gopher+writet://",store);
		store_registry.add_resource_store("gopher+writef://",store);
	}
}
