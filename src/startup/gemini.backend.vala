public class Dragonstone.Startup.Gemini.Backend {
	
	public static void setup_mimetypes(Dragonstone.SuperRegistry super_registry){
		var mimeguesser = super_registry.mimeguesser;
		mimeguesser.add_type(".gmi","text/gemini");
		mimeguesser.add_type(".gemini","text/gemini");
	}
	
	public static void setup_store(Dragonstone.SuperRegistry super_registry){
		var store = new Dragonstone.Store.Gemini();
		super_registry.stores.add_resource_store("gemini://",store);
	}
	
	public static void setup_uri_autocompletion(Dragonstone.SuperRegistry super_registry){
		var uri_autoprefixer = super_registry.uri_autoprefixer;
		uri_autoprefixer.add("gemini.","gemini://gemini.");
		uri_autoprefixer.add("gemini:","gemini://");
		uri_autoprefixer.add("gemini://","gemini://");
	}
}
