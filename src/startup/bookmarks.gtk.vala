public class Dragonstone.Startup.Bookmarks.Gtk {
	public static void setup_views(Dragonstone.SuperRegistry super_registry){
		var view_registry = super_registry.gtk_view_registry;
		var translation = super_registry.translation;
		var bookmark_registry = super_registry.bookmarks;
		print("[startup][bookmarks] setup_views\n");
		view_registry.add_view("dragonstone.bookmarks",() => {
			return new Dragonstone.GtkUi.View.Bookmarks(bookmark_registry,translation);
		});
		view_registry.add_rule(new Dragonstone.GtkUi.LegacyViewRegistryRule("interactive/bookmarks","dragonstone.bookmarks"));
	}
	
}
