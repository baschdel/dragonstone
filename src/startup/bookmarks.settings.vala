public class Dragonstone.Startup.Bookmarks.Settings {
	
	public static void register_default_settings(Dragonstone.Interface.Settings.Provider settings_provider){
		print("[startup][bookmarks][settings] register_default_settings()\n");
		settings_provider.write_object("settings.bookmarks","
			test://	The builtin Homepage
			gemini://gemini.conman.org/	The first ever gemini server
			gopher://khzae.net/	An awesome gopher server
		");
	}
}
