public class Dragonstone.GtkUi.View.Image : Gtk.ScrolledWindow, Dragonstone.GtkUi.Interface.LegacyView {
	
	private Dragonstone.Request request = null;
	private Gtk.Image image;
	private Gdk.Pixbuf pixbuf;
	
	public float factor = 1;
	public float requested_factor = 1;
	public float max_factor = 3;
	
	public bool autoscale = true;
	
	private Gtk.GestureDrag drag_gesture;
	private double drag_start_x = 0;
	private double drag_start_y = 0;
	
	private Gtk.GestureZoom zoom_gesture;
	public float zoom_origin_factor = 1;
	
	public bool display_resource(Dragonstone.Request request, Dragonstone.GtkUi.LegacyWidget.Tab tab, bool as_subview){
		if ((request.status == "success") && request.resource.mimetype.has_prefix("image/")){
			try {
				var input_stream = tab.get_file_content_stream();
				if (input_stream == null) {
					return false;
				}
				pixbuf = new Gdk.Pixbuf.from_stream(input_stream);
				image = new Gtk.Image.from_pixbuf(pixbuf);
				add(image);
				size_allocate.connect(trigger_autoscale);
			} catch (GLib.Error e) {
				return false;
			}
		} else {
			return false;
		}
		this.request = request;
		
		set_events(Gdk.EventMask.ALL_EVENTS_MASK);
		
		this.scroll_event.connect((event) => {
			if((event.state & Gdk.ModifierType.CONTROL_MASK) > 0){
				autoscale = false;
				request_scale(requested_factor-((float) event.delta_y)/10);
				return true;
			}
			return false;
		});
		this.drag_gesture = new Gtk.GestureDrag(this);
		drag_gesture.drag_begin.connect(on_drag_start);
		drag_gesture.drag_update.connect(on_drag_update);
		this.zoom_gesture = new Gtk.GestureZoom(this);
		zoom_gesture.begin.connect(on_zoom_start);
		zoom_gesture.scale_changed.connect(on_zoom_update);
		show_all();
		return true;
	}
	
	private void on_drag_start(double pos_x, double pos_y){
    drag_start_x = this.hadjustment.value;
    drag_start_y = this.vadjustment.value;
	}

	private void on_drag_update(double offset_x, double offset_y){
		  this.hadjustment.value = drag_start_x - offset_x;
		  this.vadjustment.value = drag_start_y - offset_y;
	}
	
	private void on_zoom_start(Gdk.EventSequence? sequence){
		this.zoom_origin_factor = this.factor;
	}
	
	private void on_zoom_update(double scale){
		autoscale = false;
		request_scale((float) scale*this.zoom_origin_factor);
	}
	
	public void request_scale(float new_factor){
		if (new_factor < 0.2) {
			new_factor = 0.2f;
		} else if (new_factor > max_factor) {
			new_factor = max_factor;
		}
		requested_factor = new_factor;
		this.scale(new_factor);
	}
	
	public void trigger_autoscale(Gtk.Allocation rect){
		if(autoscale){
			scale_to_window(false);			
		}
	}
	
	public void scale_to_window(bool do_not_magnify = true){
		float ph = pixbuf.get_height();
		float pw = pixbuf.get_width();
		float wh = get_allocated_height();
		float ww = get_allocated_width();
		float hr = ph/wh;
		float wr = pw/ww;
		float new_factor = factor;
		if (hr > 1 || wr > 1 || !do_not_magnify){
			new_factor = 1/float.max(hr,wr);
			//print(@"[image] rect.width=$(rect.width) rect.height=$(rect.height)\n");
			//print(@"[image] width: $pw,$ww rat: $wr | height: $ph,$wh rat: $hr\n");
			//print(@"[image] factor: $factor\n");
		}
		max_factor = new_factor*3;
		request_scale(new_factor);
	}
	
	public void scale(float factor){
		float off = this.factor/factor;
		this.factor = factor;
		int ph = pixbuf.get_height();
		int pw = pixbuf.get_width();
		//int wh = get_allocated_height();
		//int ww = get_allocated_width();
		int w = (int) (pw*factor);
		int h = (int) (ph*factor);
		if (off > 1.01 || off < 0.99){
			var scaled_pixbuf = pixbuf.scale_simple(w, h, Gdk.InterpType.NEAREST);
			if (scaled_pixbuf != null){
				image.set_from_pixbuf( (owned) scaled_pixbuf);
			}
		}
		image.set_padding(0,0);
	}
	
	public bool canHandleCurrentResource(){
		if (request == null){
			return false;
		}else{
			return (request.status == "success") && request.resource.mimetype.has_prefix("image/");
		}
	}
	
	public void cleanup(){
		this.image.clear();
		size_allocate.disconnect(trigger_autoscale);
	}
	
}
